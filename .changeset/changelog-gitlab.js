async function getReleaseLine(changeset) {
  const { stringifySummary } = await import("./changelog-processor.mjs");
  changeset.summary = changeset.summary.trim();
  return stringifySummary(changeset);
}

async function getDependencyReleaseLine(changesets, dependenciesUpdated) {
  if (dependenciesUpdated.length === 0) {
    return "";
  }
  const { stringifyDependencyUpdate } = await import(
    "./changelog-processor.mjs"
  );
  return stringifyDependencyUpdate(changesets, dependenciesUpdated);
}

module.exports = {
  getReleaseLine,
  getDependencyReleaseLine,
};
