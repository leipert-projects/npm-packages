import { text, list, listItem, link, paragraph, root } from "mdast-builder";
import remarkParse from "remark-parse";
import remarkStringify from "remark-stringify";
import { unified } from "unified";
import { VFile } from "vfile";

const stringifySHALink = ({ commit }) => {
  if (!commit) {
    return [];
  }
  return [
    text(" ("),
    link(
      `https://gitlab.com/leipert-projects/npm-packages/commit/${commit}`,
      "",
      text(commit)
    ),
    text(")"),
  ];
};

const wrapInList = () => {
  return (tree, file) => {
    const content = tree.children;
    if (!tree.children.length) {
      return;
    }

    if (content[content.length - 1].type === "paragraph") {
      content[content.length - 1].children.push(...stringifySHALink(file.data));
    } else {
      content.push(paragraph(stringifySHALink(file.data)));
    }

    tree.children = [list("unordered", [listItem(content)])];
  };
};

const processor = unified()
  .use(remarkParse)
  .use(wrapInList)
  .use(remarkStringify, {
    bullet: "-",
    fence: "`",
    fences: true,
    incrementListMarker: false,
  });

export const stringifySummary = (changeset) => {
  const vFile = new VFile(changeset.summary);
  vFile.data = changeset;

  return processor.process(vFile);
};

export const stringifyDependencyUpdate = (changesets, dependenciesUpdated) => {
  const node = root(
    list(
      "unordered",
      changesets.map((x) => {
        return listItem([
          paragraph([text("Updated dependencies"), ...stringifySHALink(x)]),
          list(
            "unordered",
            dependenciesUpdated.map(({ name, newVersion }) =>
              listItem(
                link(
                  `https://www.npmjs.com/package/${name}/v/${newVersion}`,
                  "",
                  text(`${name}@${newVersion}`)
                )
              )
            )
          ),
        ]);
      })
    )
  );

  return processor.stringify(node);
};
