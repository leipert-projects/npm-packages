# yarn-why-json

## 1.0.1

### Patch Changes

- Find a new home for this package: https://gitlab.com/leipert-projects/npm-packages.git ([539621e](https://gitlab.com/leipert-projects/npm-packages/commit/539621e))
