#!/usr/bin/env node

const program = require("commander");

let searchedPackage;
let searchedPackageVersion;

program
  .version(require("../package.json").version)
  .arguments("<package> [version]")
  .action((name, version = null) => {
    searchedPackage = name;
    searchedPackageVersion = version;
  })
  .option(
    "-l, --lock-file [path]",
    "Path to `yarn.lock` file. Defaults to current working directory",
    "yarn.lock"
  )
  .parse(process.argv);

if (!searchedPackage) {
  console.error("You need to provide a package name!");
  program.outputHelp();
  process.exit(1);
}

const fs = require("fs");
const path = require("path");

const pathToLockFile = path.isAbsolute(program.lockFile)
  ? program.lockFile
  : path.resolve(path.join(process.cwd(), program.lockFile));

let lockFileJSON;

try {
  fs.statSync(pathToLockFile);
  const parse = require("@yarnpkg/lockfile").parse;
  lockFileJSON = parse(fs.readFileSync(pathToLockFile, "utf8"));
} catch (e) {
  console.error(`Could not parse ${pathToLockFile}: \n\t${e.message}`);
  process.exit(1);
}

const whyJSON = require("./why-json");

const pathsToPackage = whyJSON(
  lockFileJSON.object,
  searchedPackage,
  searchedPackageVersion
);

process.stdout.write(JSON.stringify(pathsToPackage));
