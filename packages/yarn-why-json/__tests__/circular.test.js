const { describe, it } = require("node:test");
const { deepEqual } = require("node:assert/strict");
const fs = require("node:fs");
const lockfile = require("@yarnpkg/lockfile");

const { renderer } = require("./helper");

const whyJSON = require("../lib/why-json");

const circular = lockfile.parse(
  fs.readFileSync(__dirname + "/__fixtures__/cycle/yarn.lock", "utf8")
);

describe("circular yarn.lock,", () => {
  it("finds a properly", () => {
    const result = whyJSON(circular.object, "a");
    deepEqual(renderer(result), [["b@2.0.0", "a@1.0.0"]]);
  });

  it("finds b properly", () => {
    const result = whyJSON(circular.object, "b");
    deepEqual(renderer(result), [["a@1.0.0", "b@2.0.0"]]);
  });
});
