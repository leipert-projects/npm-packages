const { describe, it } = require("node:test");
const { deepEqual } = require("node:assert/strict");
const fs = require("node:fs");
const lockfile = require("@yarnpkg/lockfile");

const { renderer } = require("./helper");

const whyJSON = require("../lib/why-json");

const json = lockfile.parse(
  fs.readFileSync(__dirname + "/__fixtures__/complex/yarn.lock", "utf8")
);

describe("complex yarn.lock", () => {
  it("finds anymatch properly", () => {
    const result = whyJSON(json.object, "anymatch");
    deepEqual(renderer(result), [
      ["webpack-dev-server@3.1.8", "chokidar@^2.0.0", "anymatch@^2.0.0"],
    ]);
  });

  it("finds micromatch properly", () => {
    const result = whyJSON(json.object, "micromatch");
    deepEqual(renderer(result), [
      [
        "webpack-dev-server@3.1.8",
        "http-proxy-middleware@~0.18.0",
        "micromatch@^3.1.9",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "anymatch@^2.0.0",
        "micromatch@^3.1.4",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "readdirp@^2.0.0",
        "micromatch@^3.1.10",
      ],
    ]);
  });

  it("finds debug@2.6.8 properly", () => {
    const result = whyJSON(json.object, "debug", "2.6.8");
    deepEqual(renderer(result), [
      ["webpack-dev-server@3.1.8", "compression@^1.5.2", "debug@2.6.8"],
      ["webpack-dev-server@3.1.8", "portfinder@^1.0.9", "debug@^2.2.0"],
      ["webpack-dev-server@3.1.8", "sockjs-client@1.1.5", "debug@^2.6.6"],
      ["webpack-dev-server@3.1.8", "spdy@^3.4.1", "debug@^2.6.8"],
      [
        "webpack-dev-server@3.1.8",
        "spdy@^3.4.1",
        "spdy-transport@^2.0.18",
        "debug@^2.6.8",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "braces@^2.3.0",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "http-proxy-middleware@~0.18.0",
        "micromatch@^3.1.9",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "anymatch@^2.0.0",
        "micromatch@^3.1.4",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "fsevents@^1.2.2",
        "node-pre-gyp@^0.10.0",
        "needle@^2.2.1",
        "debug@^2.1.2",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "readdirp@^2.0.0",
        "micromatch@^3.1.10",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "http-proxy-middleware@~0.18.0",
        "micromatch@^3.1.9",
        "braces@^2.3.1",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "http-proxy-middleware@~0.18.0",
        "micromatch@^3.1.9",
        "extglob@^2.0.4",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "http-proxy-middleware@~0.18.0",
        "micromatch@^3.1.9",
        "nanomatch@^1.2.9",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "anymatch@^2.0.0",
        "micromatch@^3.1.4",
        "braces@^2.3.1",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "anymatch@^2.0.0",
        "micromatch@^3.1.4",
        "extglob@^2.0.4",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "readdirp@^2.0.0",
        "micromatch@^3.1.10",
        "braces@^2.3.1",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "readdirp@^2.0.0",
        "micromatch@^3.1.10",
        "extglob@^2.0.4",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "http-proxy-middleware@~0.18.0",
        "micromatch@^3.1.9",
        "extglob@^2.0.4",
        "expand-brackets@^2.1.4",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "anymatch@^2.0.0",
        "micromatch@^3.1.4",
        "extglob@^2.0.4",
        "expand-brackets@^2.1.4",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
      [
        "webpack-dev-server@3.1.8",
        "chokidar@^2.0.0",
        "readdirp@^2.0.0",
        "micromatch@^3.1.10",
        "extglob@^2.0.4",
        "expand-brackets@^2.1.4",
        "snapdragon@^0.8.1",
        "debug@^2.2.0",
      ],
    ]);
  });
});
