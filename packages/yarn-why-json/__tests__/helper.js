const renderer = (array) => {
  if (Array.isArray(array)) {
    return array.map(renderer);
  }

  return array.key ? array.key : array;
};

module.exports = {
  renderer,
};
