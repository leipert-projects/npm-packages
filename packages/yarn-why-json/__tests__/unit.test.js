const { describe, it } = require("node:test");
const { deepEqual } = require("node:assert/strict");
const fs = require("node:fs");
const lockfile = require("@yarnpkg/lockfile");

const whyJSON = require("../lib/why-json");
const renderer = require("./helper").renderer;

const json = lockfile.parse(
  fs.readFileSync(__dirname + "/__fixtures__/unit/yarn.lock", "utf8")
);

describe("unit tests whyJSON", () => {
  it("finds only the package with the exact package name", () => {
    const result = whyJSON(json.object, "ansi");
    deepEqual(renderer(result), [["ansi@^0.3.1"]]);
  });

  it("finds only the package with the exact package name and version", () => {
    const result = whyJSON(json.object, "ansi", "0.3.1");
    deepEqual(renderer(result), [["ansi@^0.3.1"]]);
  });

  it("sets the correct keys", () => {
    const result = whyJSON(json.object, "ansi", "0.3.1");
    deepEqual(
      result.map((x) => x.map(Object.keys)),
      [
        [
          [
            "key",
            "name",
            "requestedVersion",
            "version",
            "resolved",
            "integrity",
          ],
        ],
      ]
    );
  });

  it("returns an empty array for no results", () => {
    const result = whyJSON(json.object, "ansi-html", "0.3.1");
    deepEqual(renderer(result), []);
  });
});
