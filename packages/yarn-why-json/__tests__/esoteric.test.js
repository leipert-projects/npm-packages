const { describe, it } = require("node:test");
const { deepEqual, equal } = require("node:assert/strict");
const fs = require("node:fs");
const lockfile = require("@yarnpkg/lockfile");

const whyJSON = require("../lib/why-json");

const json = lockfile.parse(
  fs.readFileSync(__dirname + "/__fixtures__/esoteric/yarn.lock", "utf8")
);

describe("esoteric yarn.lock", () => {
  it("finds abbrev with `*`", () => {
    const result = whyJSON(json.object, "abbrev");
    deepEqual(result, [
      [
        {
          key: "deep@file:./deep",
          name: "deep",
          requestedVersion: "file:./deep",
          version: "1.0.0",
        },
        {
          integrity:
            "sha512-nne9/IiQ/hzIhY6pdDnbBtz7DjPTKrY00P/zvPSm5pOFkl6xuGrGnXn/VtTNNfNtAfZ9/1RtehkszU9qcTii0Q==",
          key: "abbrev@*",
          name: "abbrev",
          requestedVersion: "*",
          resolved:
            "https://registry.yarnpkg.com/abbrev/-/abbrev-1.1.1.tgz#f8f2c887ad10bf67f634f005b6987fed3179aac8",
          version: "1.1.1",
        },
      ],
    ]);
  });

  it("finds ansi-colors with URL", () => {
    const result = whyJSON(json.object, "ansi-colors");
    deepEqual(result, [
      [
        {
          key: "deep@file:./deep",
          name: "deep",
          requestedVersion: "file:./deep",
          version: "1.0.0",
        },
        {
          key: "ansi-colors@https://registry.yarnpkg.com/ansi-colors/-/ansi-colors-3.2.3.tgz",
          name: "ansi-colors",
          requestedVersion:
            "https://registry.yarnpkg.com/ansi-colors/-/ansi-colors-3.2.3.tgz",
          resolved:
            "https://registry.yarnpkg.com/ansi-colors/-/ansi-colors-3.2.3.tgz#57d35b8686e851e2cc04c403f1c00203976a1813",
          version: "3.2.3",
        },
      ],
    ]);
  });

  it("finds ansi-html with git URL", () => {
    const result = whyJSON(json.object, "ansi-html");
    deepEqual(result, [
      [
        {
          key: "deep@file:./deep",
          name: "deep",
          requestedVersion: "file:./deep",
          version: "1.0.0",
        },
        {
          key: "ansi-html@git+https://github.com/Tjatse/ansi-html.git",
          name: "ansi-html",
          requestedVersion: "git+https://github.com/Tjatse/ansi-html.git",
          resolved:
            "git+https://github.com/Tjatse/ansi-html.git#99ec49e431c70af6275b3c4e00c7be34be51753c",
          version: "0.0.7",
        },
      ],
    ]);
  });

  it("finds atob without version", () => {
    const result = whyJSON(json.object, "atob");
    deepEqual(result, [
      [
        {
          key: "deep@file:./deep",
          name: "deep",
          requestedVersion: "file:./deep",
          version: "1.0.0",
        },
        {
          integrity:
            "sha512-Wm6ukoaOGJi/73p/cl2GvLjTI5JM1k/O14isD73YML8StrH/7/lRFgmg8nICZgD3bZZvjwCGxtMOD3wWNAu8cg==",
          key: "atob@",
          name: "atob",
          requestedVersion: "",
          resolved:
            "https://registry.yarnpkg.com/atob/-/atob-2.1.2.tgz#6d9517eb9e030d2436666651e86bd9f6f13533c9",
          version: "2.1.2",
        },
      ],
    ]);
    equal(result[0][1].requestedVersion, "");
  });

  it("finds deep with file path", () => {
    const result = whyJSON(json.object, "deep");
    deepEqual(result, [
      [
        {
          key: "deep@file:./deep",
          name: "deep",
          requestedVersion: "file:./deep",
          version: "1.0.0",
        },
      ],
    ]);
  });

  it("finds lodash with version range", () => {
    const result = whyJSON(json.object, "lodash");
    deepEqual(result, [
      [
        {
          key: "deep@file:./deep",
          name: "deep",
          requestedVersion: "file:./deep",
          version: "1.0.0",
        },
        {
          integrity:
            "sha512-cQKh8igo5QUhZ7lg38DYWAxMvjSAKG0A8wGSVimP07SIUEK2UO+arSRKbRZWtelMtN5V0Hkwh5ryOto/SshYIg==",
          key: "lodash@^4.17.11",
          name: "lodash",
          requestedVersion: "^4.17.11",
          resolved:
            "https://registry.yarnpkg.com/lodash/-/lodash-4.17.11.tgz#b39ea6229ef607ecd89e2c8df12536891cac9b8d",
          version: "4.17.11",
        },
      ],
    ]);
  });

  it("finds moment with github shortcut", () => {
    const result = whyJSON(json.object, "moment");
    deepEqual(result, [
      [
        {
          key: "deep@file:./deep",
          name: "deep",
          requestedVersion: "file:./deep",
          version: "1.0.0",
        },
        {
          key: "moment@moment/moment",
          name: "moment",
          requestedVersion: "moment/moment",
          resolved:
            "https://codeload.github.com/moment/moment/tar.gz/b8f4fe2bf5bb9022655505d59b62aef12bc540d6",
          version: "2.23.0",
        },
      ],
    ]);
  });
});
