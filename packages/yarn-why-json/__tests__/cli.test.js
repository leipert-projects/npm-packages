const { describe, it } = require("node:test");
const { deepEqual, equal, match } = require("node:assert/strict");
const path = require("node:path");
const cpExec = require("child-process-promise").exec;

const { renderer } = require("./helper");

const exec = (command) => {
  return cpExec(command, { maxBuffer: 400 * 1024 });
};

const baseDir = path.join(__dirname, "..");

const program = path.join(baseDir, "lib", "cli.js");

describe("yarn-why-json cli error cases", () => {
  it("throws error if searchedPackage argument is missing", () => {
    return exec(program).catch((error) => {
      equal(error.code, 1);
      match(error.message, /You need to provide a package name/);
    });
  });

  it("throws error if yarn.lock does not exist", () => {
    return exec(`${program} --lock-file nonExistingLock.file debug`).catch(
      (error) => {
        equal(error.code, 1);
        match(error.message, /ENOENT: no such file or directory/);
      }
    );
  });

  it("throws error if yarn.lock is not parsable", () => {
    const faultyYarnLock = path.join(
      baseDir,
      "__tests__",
      "__fixtures__",
      "corrupt.yarn.lock"
    );

    return exec(`${program} --lock-file ${faultyYarnLock} debug`).catch(
      (error) => {
        equal(error.code, 1);
        match(error.message, /Invalid value type 4:0 in lockfile/);
      }
    );
  });
});

describe("yarn-why-json cli proper cases", () => {
  it("can parse empty lock files", () => {
    const lockFile = path.join(
      baseDir,
      "__tests__",
      "__fixtures__",
      "empty.yarn.lock"
    );

    return exec(`${program} --lock-file ${lockFile} debug`).then((result) => {
      equal(result.stderr, "");
      equal(result.stdout, "[]");
    });
  });

  it("returns results if the package is found", () => {
    const lockFile = path.join(
      baseDir,
      "__tests__",
      "__fixtures__",
      "unit",
      "yarn.lock"
    );

    return exec(`${program} --lock-file ${lockFile} ansi`).then((result) => {
      equal(result.stderr, "");
      const parsed = JSON.parse(result.stdout);
      deepEqual(renderer(parsed), [["ansi@^0.3.1"]]);
    });
  });
});
