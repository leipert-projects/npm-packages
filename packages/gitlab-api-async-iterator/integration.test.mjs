import GitLabAPIFactory, {
  setupGitLabAPI,
  GitLabPagedAPIIterator as GitLabPagedAPIIterator,
} from "./index.js";
import { describe, before, it } from "node:test";
import { equal, deepEqual } from "node:assert/strict";
import axios from "axios1x";

describe("Integration test", { skip: !process.env.GITLAB_TEST_TOKEN }, () => {
  const projectID = 278964;
  const webURL = "https://gitlab.com/gitlab-org/gitlab";

  const releases = [
    {
      name: "Gitlab 1.2",
      tag_name: "v1.2.0",
      created_at: "2020-04-06T16:37:16.604Z",
    },
    {
      name: "Gitlab 2.0",
      tag_name: "v2.0.0",
      created_at: "2020-04-06T16:37:15.904Z",
    },
    {
      name: "Gitlab 2.1",
      tag_name: "v2.1.0",
      created_at: "2020-04-06T16:37:15.253Z",
    },
    {
      name: "Gitlab 2.2",
      tag_name: "v2.2.0",
      created_at: "2020-04-06T16:37:14.658Z",
    },
    {
      name: "Gitlab 2.3",
      tag_name: "v2.3.0",
      created_at: "2020-04-06T16:37:14.047Z",
    },
  ];

  describe("legacy factory", () => {
    let GitLabAPI, GitLabAPIIterator;

    before(() => {
      const build = GitLabAPIFactory({
        privateToken: process.env.GITLAB_TEST_TOKEN,
      });
      GitLabAPI = build.GitLabAPI;
      GitLabAPIIterator = build.GitLabAPIIterator;
    });

    it("GitLabAPI: works as expected", async () => {
      const { data } = await GitLabAPI.get(`/projects/${projectID}`);

      equal(data.id, projectID);
      equal(data.web_url, webURL);
    });

    it("GitLabPagedAPIIterator: works as expected", async () => {
      const releaseIterator = new GitLabAPIIterator(
        `/projects/${projectID}/releases`,
        {
          order_by: "released_at",
          sort: "asc",
          page: 1,
          maxPages: 6,
          per_page: 1,
        }
      );

      const result = [];

      for await (const { tag_name, name, created_at } of releaseIterator) {
        result.push({ tag_name, name, created_at });
      }

      equal(result.length, 5);
      deepEqual(result, releases);
    });
  });

  describe("new factory", () => {
    let GitLabAPI;

    before(() => {
      GitLabAPI = setupGitLabAPI(axios, {
        privateToken: process.env.GITLAB_TEST_TOKEN,
      });
    });

    it("GitLabAPI: works as expected", async () => {
      const { data } = await GitLabAPI.get(`/projects/${projectID}`);

      equal(data.id, projectID);
      equal(data.web_url, webURL);
    });

    it("GitLabPagedAPIIterator: works as expected", async () => {
      const releaseIterator = new GitLabPagedAPIIterator(
        GitLabAPI,
        `/projects/${projectID}/releases`,
        {
          order_by: "released_at",
          sort: "asc",
          page: 1,
          maxPages: 6,
          per_page: 1,
        }
      );

      const result = [];

      for await (const { tag_name, name, created_at } of releaseIterator) {
        result.push({ tag_name, name, created_at });
      }

      equal(result.length, 5);
      deepEqual(result, releases);
    });
  });
});
