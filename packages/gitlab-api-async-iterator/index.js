const GitLabAPIFactory = require("./lib/apiFactory");
const GitLabAPIIteratorFactory = require("./lib/iteratorFactory");

/**
 * @deprecated
 * @param options
 */
function create(options) {
  const GitLabAPI = GitLabAPIFactory(options);
  const GitLabAPIIterator = GitLabAPIIteratorFactory(GitLabAPI);

  return { GitLabAPI, GitLabAPIIterator };
}

create.GitLabAPIFactory = GitLabAPIFactory;
create.GitLabAPIIteratorFactory = GitLabAPIIteratorFactory;

module.exports = create;

module.exports.setupGitLabAPI = GitLabAPIFactory.setupGitLabAPI;
module.exports.GitLabPagedAPIIterator =
  GitLabAPIIteratorFactory.GitLabPagedAPIIterator;
