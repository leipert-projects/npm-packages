# Change Log

## 1.3.1

### Patch Changes

- Fix legacy factory if no options are provided ([3664873](https://gitlab.com/leipert-projects/npm-packages/commit/3664873))

## 1.3.0

### Minor Changes

- Deprecate old way of creating GitLab API / Iterator.

  A future version of this project will not ship `axios` as a dependency.
  Therefore one needs to provide an axios instance from the outside.
  The APIs have been adjusted. Have a look at this example to see how the code needs to be migrated.

  Setup:

  ```js
  // Before: A factory was imported...
  import factory from "gitlab-api-async-iterator";
  // in order to create an axios instance and an iterator
  const { GitLabAPI, GitLabAPIIterator } = factory(options);

  // After: You manually import axios and the setup / iterator
  import axios from "axios";
  import {
    setupGitLabAPI,
    GitLabPagedAPIIterator,
  } from "gitlab-api-async-iterator";
  // Side-Note: You do not need to pass the default axios instance, but can create a custom with axios.create()
  const GitLabAPI = setupGitLabAPI(axios, options);
  ```

  The usage of the GitLabAPI stays the same. Usage for the iterator change slightly

  ```js
  // Before
  const groupIterator = new GitLabAPIIterator("/groups");
  for await (const group of groupIterator) {
    console.log("Group Details:", group);
  }

  /* After, you need to pass the axios instance created with setupGitLabAPI
   * This also means that you could use any axios instance for GitLabPagedAPIIterator though,
   * and not necessarily one created by the package:
   */
  const groupIterator = new GitLabPagedAPIIterator(GitLabAPI, "/groups");
  for await (const group of groupIterator) {
    console.log("Group Details:", group);
  }
  ```

  If you extended the GitLabAPIIterator class, you could change the script the following way:

  ```js
  // Before
  class PipelineIterator extends GitLabAPIIterator {
    constructor(projectId, options) {
      super(`/projects/${projectId}/pipelines`, options);
    }
  }

  // After
  class PipelineIterator extends GitLabPagedAPIIterator {
    constructor(projectId, options) {
      super(GitLabAPI, `/projects/${projectId}/pipelines`, options);
    }
  }
  ```

  &#x20;([d08eb8f](https://gitlab.com/leipert-projects/npm-packages/commit/d08eb8f))

- Widen support for axios version. Now supported and tested with versions >= 0.24 && < 2.0.0. ([4cb49e6](https://gitlab.com/leipert-projects/npm-packages/commit/4cb49e6))

## 1.2.1

### Patch Changes

- This is just a test

# [1.2.0](https://gitlab.com/leipert-projects/npm-packages/compare/gitlab-api-async-iterator@1.1.0...gitlab-api-async-iterator@1.2.0) (2021-10-29)

### Features

- Bump axios ([01f2246](https://gitlab.com/leipert-projects/npm-packages/commit/01f22468e6e723342ec197cd665809e4d8aa55b0))

# 1.1.0 (2020-08-25)

### Bug Fixes

- **gitlab-api-async-iterator:** Deal with non-node environments ([6de110b](https://gitlab.com/leipert-projects/npm-packages/commit/6de110b5c1e55b1dead3673fad656f52f6b94e96))

### Features

- **gitlab-api-async-iterator:** Add Retry-After header to retry error handling ([ff18714](https://gitlab.com/leipert-projects/npm-packages/commit/ff18714c42e26515fb89822e3b7009cfc0de4603))

# 1.0.0 (2020-08-23)

### Features

- **gitlab-api-async-iterator:** Initial implementation ([133a4d1](https://gitlab.com/leipert-projects/npm-packages/commit/133a4d16ed257768a7f9cc8f9a91ee98005141eb))
