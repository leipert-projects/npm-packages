import { describe, it, beforeEach, mock } from "node:test";
import { deepEqual, equal, rejects } from "node:assert/strict";
import axios024 from "axios024";
import axios025 from "axios025";
import axios026 from "axios026";
import axios027 from "axios027";
import axios1x from "axios1x";
import nock from "nock";
import { setupGitLabAPI } from "./apiFactory.js";

function testAPIFactory(axios) {
  const baseURL = "https://example.org/";
  const privateToken = "FOO";

  return () => {
    beforeEach(() => {
      delete process.env.GITLAB_TOKEN;
      delete process.env.DANGER_GITLAB_API_TOKEN;
    });

    it("throws if no token provided", async () => {
      await rejects(
        async () => {
          return setupGitLabAPI(axios);
        },
        {
          message:
            "Please provide a GitLab token as an ENV variable via GITLAB_TOKEN or DANGER_GITLAB_API_TOKEN",
        }
      );
    });

    describe("tokens", () => {
      let scope;
      beforeEach(() => {
        scope = nock(baseURL, {
          reqheaders: {
            "PRIVATE-TOKEN": privateToken,
          },
        })
          .get("/")
          .reply(200, "test response");
      });

      it("uses token provided via options", async () => {
        const api = setupGitLabAPI(axios, { privateToken, baseURL });
        await api.get(baseURL);

        scope.done();
      });

      it("uses token provided via GITLAB_TOKEN env", async () => {
        process.env.GITLAB_TOKEN = privateToken;
        const api = setupGitLabAPI(axios, { baseURL });
        await api.get(baseURL);

        scope.done();
      });

      it("uses token provided via DANGER_GITLAB_API_TOKEN env", async () => {
        process.env.GITLAB_TOKEN = privateToken;
        const api = setupGitLabAPI(axios, { baseURL });
        await api.get(baseURL);

        scope.done();
      });
    });

    describe("API endpoint", () => {
      it("defaults to GitLab.com API", async () => {
        // Set up the mock request.
        const scope = nock("https://gitlab.com/api/v4/")
          .get("/test")
          .reply(200, "test response");
        const api = setupGitLabAPI(axios, { privateToken });

        const { data } = await api.get("/test");

        equal(data, "test response");

        // Assert that the expected request was made.
        scope.done();
      });

      it("utilizes baseURL provided", async () => {
        // Set up the mock request.
        const scope = nock(baseURL).get("/test").reply(200, "test response");
        const api = setupGitLabAPI(axios, { privateToken, baseURL });

        const { data } = await api.get("/test");

        equal(data, "test response");

        // Assert that the expected request was made.
        scope.done();
      });
    });

    it("works with axios `request` method", async () => {
      const scope = nock(baseURL).get("/get").reply(200, "");
      const api = setupGitLabAPI(axios, { privateToken, baseURL });

      await api.request({ url: "/get", method: "get" });

      // Assert that the expected request was made.
      scope.done();
    });

    it("works with all the HTTP methods", async () => {
      const scope = nock(baseURL)
        .get("/get")
        .reply(200, "")
        .patch("/patch")
        .reply(200, "")
        .delete("/delete")
        .reply(200, "")
        .head("/head")
        .reply(200, "")
        .options("/options")
        .reply(200, "")
        .post("/post")
        .reply(200, "")
        .put("/put")
        .reply(200, "");
      const api = setupGitLabAPI(axios, { privateToken, baseURL });

      await api.get("/get");
      await api.patch("/patch");
      await api.delete("/delete");
      await api.head("/head");
      await api.options("/options");
      await api.post("/post");
      await api.put("/put");

      // Assert that the expected request was made.
      scope.done();
    });

    function retriesSuite(errorCode) {
      return () => {
        const exponentialBackoff = [
          100, 200, 400, 800, 1600, 3200, 6400, 12800, 25600, 51200,
        ];

        const testWithRetries = (EXPECTED_RETRIES, retries) => {
          return async () => {
            const intervals = [];
            setTimeout = mock.fn((_fn, ms) => {
              intervals.push(ms);
              _fn();
            });
            const scope = nock(baseURL)
              .get("/get")
              .times(EXPECTED_RETRIES + 1)
              .reply(errorCode, "");

            const api = setupGitLabAPI(axios, {
              privateToken,
              baseURL,
              maxRetries: retries,
            });

            await rejects(async () => {
              return api.get("/get");
            }, Error);

            equal(setTimeout.mock.callCount(), EXPECTED_RETRIES);
            deepEqual(intervals, exponentialBackoff.slice(0, EXPECTED_RETRIES));
            scope.done();
          };
        };

        it(
          "retries 5 times per default, with exponential backoff",
          testWithRetries(5)
        );
        it(
          "retries 3 times if set, with exponential backoff",
          testWithRetries(3, 3)
        );
        it(
          "retries 10 times if set, with exponential backoff",
          testWithRetries(10, 10)
        );
      };
    }

    describe("retries on 429 errors", retriesSuite(429));
    describe("retries on 500 errors", retriesSuite(500));
    describe("retries on 502 errors", retriesSuite(502));
    describe("retries on 503 errors", retriesSuite(503));
    describe("retries on 505 errors", retriesSuite(504));
  };
}

describe("apiFactory: axios@0.24", testAPIFactory(axios024));
describe("apiFactory: axios@0.25", testAPIFactory(axios025));
describe("apiFactory: axios@0.26", testAPIFactory(axios026));
describe("apiFactory: axios@0.27", testAPIFactory(axios027));
describe("apiFactory: axios@1.x", testAPIFactory(axios1x));
