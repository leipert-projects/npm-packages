#!/usr/bin/env node
const readline = require("readline");
const axios = require("axios");
const asyncAsyncPool = require("./asyncAsyncPool");
const {
  setupGitLabAPI,
  GitLabPagedAPIIterator,
} = require("gitlab-api-async-iterator");

const JOB_STATES = {
  RUNNING: "running",
  FAILED: "failed",
  PENDING: "pending",
  CREATED: "created",
  SUCCESS: "success",
};

const API_FILTER_STATES = [
  JOB_STATES.RUNNING,
  JOB_STATES.FAILED,
  JOB_STATES.PENDING,
  JOB_STATES.CREATED,
];

const gitlabAPI = setupGitLabAPI(axios);

const JEST_TYPE = "jest";
const RSPEC_TYPE = "rspec";

class JobIterator extends GitLabPagedAPIIterator {
  constructor(projectPath, pipelineId) {
    super(
      gitlabAPI,
      `/projects/${encodeURIComponent(
        projectPath
      )}/pipelines/${pipelineId}/jobs`,
      { per_page: 50, scope: API_FILTER_STATES }
    );
  }
}

class BridgeIterator extends GitLabPagedAPIIterator {
  constructor(projectPath, pipelineId) {
    super(
      gitlabAPI,
      `/projects/${encodeURIComponent(
        projectPath
      )}/pipelines/${pipelineId}/bridges`,
      { per_page: 50, scope: API_FILTER_STATES }
    );
  }
}

const SECTION_START = /section_start:.+:step_script/;
const SECTION_END = /section_end:.+:step_script/;

async function processTrace(stream, processor) {
  const rl = readline.createInterface({
    input: stream,
    crlfDelay: Infinity,
  });
  let started = false;
  let response = [];
  for await (const line of rl) {
    if (!started) {
      started = SECTION_START.test(line);
      continue;
    }
    const result = processor(line);
    if (result) {
      response.push(result);
    }

    if (SECTION_END.test(line)) {
      rl.close();
      return response;
    }
  }
  return response;
}

function sortUnique(arr) {
  return [...new Set(arr)].sort();
}

function printUsage() {
  return [
    "Usage: gitlab-spec-failures <pipeline_web_url | merge_request_web_url> ",
    "",
    "Please provide a URL of the format: https://gitlab.com/<project_path>/-/pipelines/<pipeline_id>",
    " or https://<project_path>/gitlab/-/merge_requests/<mr_iid>",
  ].join("\n");
}

async function main() {
  const GitLabMRRegex = /https:\/\/gitlab.com\/(.+)\/-\/merge_requests\/(\d+)/;
  const GitLabPipelineRegex = /https:\/\/gitlab.com\/(.+)\/-\/pipelines\/(\d+)/;

  const mrURL = process.argv.find((x) => GitLabMRRegex.test(x));
  const pipelineURL = process.argv.find((x) => GitLabPipelineRegex.test(x));

  if (!pipelineURL && !mrURL) {
    console.warn(printUsage());
    throw new Error("URL argument missing");
  }

  let projectPath, parentPipeline, mrIid;

  if (pipelineURL) {
    [, projectPath, parentPipeline] = pipelineURL.match(GitLabPipelineRegex);
    console.log(`Retrieving data for Pipeline: ${pipelineURL}`);
  } else {
    [, projectPath, mrIid] = mrURL.match(GitLabMRRegex);
    console.log(`Retrieving data for MR: ${mrURL}`);
    const { data: pipelines } = await gitlabAPI.get(
      `/projects/${encodeURIComponent(
        projectPath
      )}/merge_requests/${mrIid}/pipelines`
    );
    if (!pipelines.length) {
      throw new Error(`MR ${mrURL} doesn't have a pipeline yet`);
    }
    const latest = pipelines.sort((b, a) => a.id - b.id)[0];
    parentPipeline = latest.id;
    console.log(
      `Found ${pipelines.length} pipelines, latest seems to be: ${latest.web_url}`
    );
  }

  async function processJob(job) {
    const { id, name, web_url, status } = job;

    if (
      !(
        name.startsWith("rspec-ee ") ||
        name.startsWith("rspec ") ||
        name.startsWith("jest ")
      )
    ) {
      return {
        running: false,
      };
    }

    if (
      [JOB_STATES.PENDING, JOB_STATES.RUNNING, JOB_STATES.CREATED].includes(
        status
      )
    ) {
      console.log(
        `'${name}' is not finished yet, status: ${status} (${web_url})`
      );
      return { running: true };
    }

    if (status === JOB_STATES.SUCCESS) {
      return {
        running: false,
      };
    }

    const traceURL = `/projects/${encodeURIComponent(
      projectPath
    )}/jobs/${id}/trace`;

    const { data: traceStream } = await gitlabAPI.get(traceURL, {
      responseType: "stream",
    });

    console.log(
      `Retrieved trace for: '${name}', status: ${status} (${web_url})`
    );

    if (name.startsWith("jest")) {
      return {
        type: JEST_TYPE,
        files: await processTrace(
          traceStream,
          (line) => line.match(/FAIL (\S+)/)?.[1]
        ),
      };
    }
    return {
      type: RSPEC_TYPE,
      files: await processTrace(
        traceStream,
        (line) => line.match(/rspec[\s']+(\..+?)[[:]/)?.[1]
      ),
    };
  }

  const failures = {
    [JEST_TYPE]: [],
    [RSPEC_TYPE]: [],
  };
  let running = false;

  const bridgeIterator = new BridgeIterator(projectPath, parentPipeline);

  const pipelineIDs = [parentPipeline];
  for await (const bridge of bridgeIterator) {
    const { pipeline, downstream_pipeline, name } = bridge;
    if (
      downstream_pipeline &&
      pipeline?.project_id === downstream_pipeline.project_id
    ) {
      console.log(
        `Checking child pipeline: ${name} (${downstream_pipeline.web_url})`
      );
      pipelineIDs.push(downstream_pipeline.id);
    }
  }

  for (const id of pipelineIDs) {
    const iterator = new JobIterator(projectPath, id);

    for await (const result of asyncAsyncPool(50, iterator, processJob)) {
      const { type, files, running: jobRunning = false } = result;
      running ||= jobRunning;
      if (files && type) {
        failures[type].push(...files);
      }
    }
  }

  const failedJestFiles = sortUnique(failures[JEST_TYPE]);
  const failedRspecFiles = sortUnique(failures[RSPEC_TYPE]);

  console.log("# ".repeat(60));

  if (failedJestFiles.length > 0) {
    console.log(
      `Jest command to run ${failedJestFiles.length} failing files:\n`
    );

    console.log("yarn run jest -u --watch " + failedJestFiles.join(" \\\n  "));
  } else {
    console.log(
      `No jest failures found in pipeline ${parentPipeline} - hooray!`
    );
  }

  console.log("# ".repeat(60));

  if (failedRspecFiles.length > 0) {
    console.log(
      `Rspec command to run ${failedRspecFiles.length} failing files:\n`
    );

    console.log("./bin/spring rspec " + failedRspecFiles.join(" \\\n  "));
  } else {
    console.log(
      `No rspec failures found in pipeline ${parentPipeline} - hooray!`
    );
  }

  if (running) {
    console.log("At least one job is still running (see above)");
  }
}

if (process.argv.includes("--help" || "-h")) {
  console.log(printUsage());
  process.exit(0);
}

main().catch((e) => {
  console.error(e);
  process.exit(1);
});
