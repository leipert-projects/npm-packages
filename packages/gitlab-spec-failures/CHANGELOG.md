# Change Log

## 1.2.0

### Minor Changes

- Support iterating over child pipelines ([732917a](https://gitlab.com/leipert-projects/npm-packages/commit/732917a))

## 1.1.1

### Patch Changes

- Ensure new helper JS is shipped ([5c3355f](https://gitlab.com/leipert-projects/npm-packages/commit/5c3355f))

## 1.1.0

### Minor Changes

- Improve performance and make rspec parsing a little more robust ([3b79f9e](https://gitlab.com/leipert-projects/npm-packages/commit/3b79f9e))

## 1.0.2

### Patch Changes

- Updated dependencies ([3664873](https://gitlab.com/leipert-projects/npm-packages/commit/3664873))

  - [gitlab-api-async-iterator@1.3.1](https://www.npmjs.com/package/gitlab-api-async-iterator/v/1.3.1)

## [1.0.1](https://gitlab.com/leipert-projects/npm-packages/compare/@leipert/gitlab-spec-failures@1.0.0...@leipert/gitlab-spec-failures@1.0.1) (2021-10-29)

**Note:** Version bump only for package @leipert/gitlab-spec-failures

# 1.0.0 (2020-08-25)

### Bug Fixes

- **gitlab-spec-failures:** Fix output of jest and rspec failures ([2ac703f](https://gitlab.com/leipert-projects/npm-packages/commit/2ac703f25b3b6756f4b8a690a952b360cadda84a))

### Features

- **gitlab-spec-failures:** Initial implementation ([52767d6](https://gitlab.com/leipert-projects/npm-packages/commit/52767d65481575cf0359b481c610ae86d47f5c77))
