#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

git status
CI="${CI:-false}"
DEFAULT_BRANCH="${CI_DEFAULT_BRANCH:-main}"

if [ "$CI" == "true" ]; then
  CURRENT_BRANCH="${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME:-${CI_COMMIT_REF_NAME:-}}"
  HEAD_SHA="${CI_MERGE_REQUEST_SOURCE_BRANCH_SHA:-${CI_COMMIT_SHA:-}}"
  echo "CURRENT_BRANCH: $CURRENT_BRANCH"
  echo "HEAD_SHA: $HEAD_SHA"
  git fetch
  git branch -D "$CURRENT_BRANCH" || echo "branch doesn't exist locally"
  git checkout "$CURRENT_BRANCH"
  CURR_SHA=$(git rev-parse --verify HEAD)
  if [ "$CURR_SHA" != "$HEAD_SHA" ]; then
    echo "CURR_SHA ($CURR_SHA) and HEAD_SHA ($HEAD_SHA) do not match. Error out."
    exit 1
  fi
else
  CURRENT_BRANCH=$(git name-rev --name-only HEAD)
fi

yarn changeset status --verbose || exit 0
yarn changeset status --output=./.changeset/status.json
if node -e "process.exitCode = require('./.changeset/status.json').releases.length ? 0 : 1"; then
  if [ "$CI" == "true" ] && [ "$CURRENT_BRANCH" == "$DEFAULT_BRANCH" ]; then
    echo "Setting credentials"
    git config --global user.name "Lukas Eipert"
    git config --global user.email "git@leipert.io"
    git remote set-url --push origin "https://${GL_PUSH_USER}:${GL_PUSH_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git"
    npm config set -- '//registry.npmjs.org/:_authToken' "${NPM_TOKEN}"
    echo "Applying changesets"
    yarn changeset version
    git add .
    git commit -m "Updating packages for release"
    yarn changeset publish
    git push
    git push --tags
  else
    echo "Preview of diff (dry run of publishing)"
    yarn changeset version
    git diff
  fi
else
  echo "No changes require a release"
fi